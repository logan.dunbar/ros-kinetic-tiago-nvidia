# Dockerized ROS Kinetic with TIAGo src for Nvidia-based Ubuntu

This project provides a ready-to-use installation of ROS Kinetic Kame in an Ubuntu 16.04 container with Nvidia acceleration.

## Getting Started

Visit https://gitlab.com/logan.dunbar/ros-kinetic-tiago-nvidia to download the source code

### Prerequisites

- Ubuntu or *nix with X windows.
- Latest Nvidia driver
- [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) (don't forget the Ubuntu [post-install steps](https://docs.docker.com/install/linux/linux-postinstall/), otherwise replace all `docker` commands with `sudo docker`)
- [nvidia-docker2](https://github.com/nvidia/nvidia-docker/wiki/Installation-(version-2.0))

### Installing

Fetch the code

```
git clone git@gitlab.com:logan.dunbar/ros-kinetic-tiago-nvidia.git
```

Move into folder

```
cd ros-kinetic-tiago-nvidia
```

Fetch all the TIAGo submodules

```
git submodule update --init --recursive
```

Make the runnable files executable

```
chmod +x *.sh
```

Build the docker image (will take a while)

```
docker build -t ros-kinetic-tiago-nvidia .

(non-nvidia: docker build -t ros-kinetic-tiago-non-nvidia -f non_nvidia/Dockerfile .)
```

Spin up your fresh copy of ROS with TIAGo! (Will take a while first time due to building TIAGo source)

```
./run.sh

(non-nvidia: ./run_non_nvidia.sh)
```

If all has gone according to plan you should be seeing a blinking cursor with something like this

```
root@501e97592fb5:/catkin_ws# 
```

To test if ROS and TIAGo work on your system, please try running the following 

```
roslaunch tiago_gazebo tiago_gazebo.launch public_sim:=true robot:=titanium
```

and TIAGo should successfully tuck his arm with a message saying  `Tuck arm... Arm tucked. [tuck_arm-23] process has finished cleanly.`

To connect a new terminal to the running docker container

```
docker exec -it $(docker ps -q -f ancestor=ros-kinetic-tiago-nvidia -f status=running) /bin/bash
```

or `docker container ls`, copy the ID, and then `docker exec -it <ID> /bin/bash`

To remove exited containers (WARNING: removes ALL exited containers)

```
docker rm $(docker ps -q -f status=exited)
```

## Contributing



## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Logan Dunbar** - *Initial work* - [Logan Dunbar](https://gitlab.com/logan.dunbar)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

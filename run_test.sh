#!/bin/bash

# XAUTH=/tmp/.docker.xauth
# touch $XAUTH
# xauth extract - $DISPLAY | xauth -f $XAUTH merge -
# chmod a+r $XAUTH

xhost +
docker run -it \
    --env="DISPLAY" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --device="/dev/dri:/dev/dri" \
    opengl-test \
    bash
    # --volume="/usr/lib:/usr/lib" \
    # --volume="$XAUTH:$XAUTH" \
    # --env="XAUTHORITY=$XAUTH" \
    # --env="QT_X11_NO_MITSHM=1" \
    # --volume="/usr/lib/x86_64-linux-gnu/nvidia:/usr/lib/x86_64-linux-gnu/nvidia" \
    # --device=/dev/dri/renderD128 \
    # --privileged \
    # --runtime=nvidia \
#!/bin/bash

XAUTH=/tmp/.docker.xauth
touch $XAUTH
xauth extract - $DISPLAY | xauth -f $XAUTH merge -
chmod a+r $XAUTH

docker run -it \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --env="XAUTHORITY=$XAUTH" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume="$XAUTH:$XAUTH" \
    --volume="$(pwd):/catkin_ws" \
    --cpus=4 \
    --runtime=nvidia \
    ros-kinetic-tiago-nvidia \
    bash
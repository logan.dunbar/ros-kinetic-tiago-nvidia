FROM nvidia/opengl:1.1-glvnd-devel-ubuntu16.04

# install packages
RUN apt-get update && apt-get install -q -y \
    dirmngr \
    gnupg2 \
    lsb-release

# setup keys
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 421C365BD9FF1F717815A3895523BAEEB01FA116

# setup sources.list
RUN echo "deb http://packages.ros.org/ros/ubuntu `lsb_release -sc` main" > /etc/apt/sources.list.d/ros-latest.list

# update apt
RUN apt-get update

# install bootstrap tools
RUN apt-get install --no-install-recommends -y \
    python-rosdep \
    python-rosinstall \
    python-rosinstall-generator \
    python-vcstools \
    python-wstool \
    build-essential \
    git

# setup environment
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

# bootstrap rosdep
RUN rosdep init && rosdep update

# set ROS distro
ENV ROS_DISTRO kinetic

# install ros packages
RUN apt-get install -y ros-kinetic-desktop-full=1.3.2-0*

# install TIAGo specific ROS packages
RUN apt-get install -q -y \
    python-catkin-tools \
    ros-kinetic-joint-state-controller \
    ros-kinetic-twist-mux \
    ros-kinetic-ompl \
    ros-kinetic-controller-manager \
    ros-kinetic-moveit-core \
    ros-kinetic-moveit-ros-perception \
    ros-kinetic-moveit-ros-move-group \
    ros-kinetic-moveit-kinematics \
    ros-kinetic-moveit-ros-planning-interface \
    ros-kinetic-moveit-simple-controller-manager \
    ros-kinetic-moveit-planners-ompl \
    ros-kinetic-joy \
    ros-kinetic-joy-teleop \
    ros-kinetic-teleop-tools \
    ros-kinetic-control-toolbox \
    ros-kinetic-sound-play \
    ros-kinetic-navigation \
    ros-kinetic-eband-local-planner \
    ros-kinetic-depthimage-to-laserscan \
    ros-kinetic-openslam-gmapping \
    ros-kinetic-gmapping \
    ros-kinetic-moveit-commander

# creates and sets the directory for all the next commands
WORKDIR /catkin_ws

# copy tiago source code in
COPY src src

# fill in any missing ROS packages
RUN ["/bin/bash", "-c", "source /opt/ros/kinetic/setup.bash && rosdep install -y --from-paths src --ignore-src --rosdistro kinetic --skip-keys=\"opencv2 opencv2-nonfree pal_laser_filters speed_limit sensor_to_cloud hokuyo_node libdw-dev\""]

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

# setup entrypoint
COPY ./ros_entrypoint.sh /


# clean up
RUN rm -rf /var/lib/apt/lists/*

# change directory on start
RUN echo "cd /catkin_ws" >> /root/.bashrc

# source ROS environment
RUN echo "source /opt/ros/$ROS_DISTRO/setup.bash" >> /root/.bashrc
RUN echo "source /catkin_ws/src/setup.bash" >> /root/.bashrc
RUN echo "source /catkin_ws/devel/setup.bash" >> /root/.bashrc

ENTRYPOINT ["/ros_entrypoint.sh"]
CMD ["bash"]
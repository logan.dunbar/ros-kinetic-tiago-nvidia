#!/bin/bash
set -e

# setup ros environment
source "/opt/ros/$ROS_DISTRO/setup.bash"
source "/catkin_ws/src/setup.bash"

# build tiago
# catkin build -DCATKIN_ENABLE_TESTING=0

exec "$@"